from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf
import numpy as np
from PIL import Image

#read input data from MNIST_data/ and enable one_hot option
mnist = input_data.read_data_sets("MNIST_data/",one_hot = True)

#creat placeholder of input data: x and input label: y_
x = tf.placeholder("float",shape = [None,784])
y_ = tf.placeholder("float",shape = [None,10])

w = tf.Variable(tf.truncated_normal(shape=[784,10], dtype=tf.float32, name='w'))
b = tf.Variable(tf.truncated_normal(shape=[10], dtype=tf.float32, name='b'))

saver = tf.train.Saver()

with tf.Session() as sess:
    saver.restore(sess, "model/softmax.ckpt")
    print("read model successfully")
    #convert image's numpy arrary to a tensor
    for i in range(100):
        im_data = np.array(np.reshape(mnist.train.images[i], [28, 28]) * 255, dtype=np.float32)
        x = tf.convert_to_tensor(im_data)
        x = tf.reshape(x, [1, 784])
        y = tf.nn.softmax(tf.matmul(x, w) + b)
        # convert to python list
        output = list(y.eval(session=sess))[0]
        output = output.tolist()
        real = list(mnist.train.labels[i])
        print(output.index(max(output)) + 1,' and ', real.index(max(real)) + 1)
