from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf

# read input data from MNIST_data/ and enable one_hot option
mnist = input_data.read_data_sets("MNIST_data/",one_hot = True)

# new a session
sess = tf.InteractiveSession()

# creat placeholder of input data: x and input label: y_
x = tf.placeholder("float",shape = [None,784])
y_ = tf.placeholder("float",shape = [None,10])
keep_prob = tf.placeholder("float")

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1,1,1,1], padding='SAME')

def max_pool_2x2(x):
    return tf.nn.max_pool(x, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

def norm(x):
    return tf.nn.lrn(x, 4, bias=1.0, alpha=0.01/9.0, beta=0.75)

W_conv_1 = weight_variable([5,5,1,32])
b_conv_1 = bias_variable([32])

x_image = tf.reshape(x, [-1,28,28,1])
h_conv_1 = tf.nn.relu(conv2d(x_image, W_conv_1) + b_conv_1)
h_pool_1 = max_pool_2x2(h_conv_1)
h_norm_1 = norm(h_pool_1)
h_norm_1 = tf.nn.dropout(h_norm_1, keep_prob)

W_conv_2 = weight_variable([5,5,32,64])
b_conv_2 = bias_variable([64])

h_conv_2 = tf.nn.relu(conv2d(h_norm_1, W_conv_2) + b_conv_2)
h_pool_2 = max_pool_2x2(h_conv_2)
h_norm_2 = norm(h_pool_2)
h_norm_2 = tf.nn.dropout(h_norm_2, keep_prob)

W_fc_1 = weight_variable([7*7*64,1024])
b_fc_1 = bias_variable([1024])

h_pool_2_flat = tf.reshape(h_norm_2, [-1,7*7*64])
h_fc_1 = tf.nn.relu(tf.matmul(h_pool_2_flat, W_fc_1) + b_fc_1)

h_fc_drop = tf.nn.dropout(h_fc_1, keep_prob)

W_fc_2 = weight_variable([1024,10])
b_fc_2 = bias_variable([10])

y_conv = tf.nn.softmax(tf.matmul(h_fc_drop, W_fc_2) + b_fc_2)

cross_entropy = -tf.reduce_sum(y_*tf.log(y_conv))
train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))

saver = tf.train.Saver()

if __name__ == '__main__':
    sess.run(tf.global_variables_initializer())
    for i in range(24000):
        batch = mnist.train.next_batch(50)
        if i%100 == 0:
            train_accuracy = accuracy.eval(feed_dict={
                x:batch[0], y_:batch[1], keep_prob:1.0
            })
            print("step %d, training accuracy = %g"%(i, train_accuracy))
        train_step.run(feed_dict={x:batch[0], y_:batch[1], keep_prob:0.5})

    print("test accuracy = %g"%accuracy.eval(feed_dict={
        x:mnist.test.images, y_:mnist.test.labels, keep_prob:1.0
    }))

    save_path = saver.save(sess, "model/cnn.ckpt")
    print("model has been saved into %s"%save_path)
